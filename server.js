
if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()
}

const express = require('express')
const app = express()
const bcrypt = require('bcrypt')
// const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
// const methodOverride = require('method-override')
const admin = require('firebase-admin')
const firebase = require('firebase')
const serviceAccount = require("./serviceAccountKey.json")
// import firebase from 'firebase';
const UUID = require('uuid')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://flutter-chat-387a5.firebaseio.com"
});


const firebaseConfig = {
    apiKey: "AIzaSyD7yyzjleVt61jlCwhe0L6sXqECu06ywVM",
    authDomain: "flutter-chat-387a5.firebaseapp.com",
    databaseURL: "https://flutter-chat-387a5.firebaseio.com",
    projectId: "flutter-chat-387a5",
    storageBucket: "flutter-chat-387a5.appspot.com",
    messagingSenderId: "74829399015",
    appId: "1:74829399015:web:ab7b61ccb096cea90d7342"
};
firebase.initializeApp(firebaseConfig);
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.NONE)

const fbdatabase = firebase.database().ref().child('UserProfile')
const prod_db = firebase.database().ref().child('ProductList')


// const initializePassport = require('./passport-config')
// initializePassport( 
//     passport, 
//     email => users.find( user => user.email === email ),
//     id => users.find( user => user.id === id )
// )






const users = []

// app.use( function( req, res, next ) {
//     res.status(404).render('login.ejs')
// })

app.set('view-engine', 'ejs')
app.use(express.static('views'))
app.use(express.urlencoded({ extended: false }))
app.use(flash())
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))


// app.use(passport.initialize())
// app.use(passport.session())
// app.use(methodOverride('_method'))



app.get('/', checkAuthenticated, (req, res) => {
    return res.render('index.ejs')
    res.end()
})


app.get('/settings', checkAuthenticated, (req, res) => {
    return res.render('settings.ejs')
    res.end()
})


app.get('/grocery', checkAuthenticated, (req, res) => {
    prod_db.once("value", function(snapshot) {
        return res.render('grocery.ejs', { products: (snapshot.val())['Grocery']['Items'] })
        res.end()
    })
})

app.get('/clothes', checkAuthenticated, (req, res) => {
    prod_db.once("value", function(snapshot) {
        return res.render('clothes.ejs', { products: (snapshot.val())['Clothes']['Items'] })
        res.end()
    })
})

app.get('/electronics', checkAuthenticated, (req, res) => {
    prod_db.once("value", function(snapshot) {
        return res.render('electronics.ejs', { products: (snapshot.val())['Electronics']['Items'] })
        res.end()
    })
})


app.get('/cosmetics', checkAuthenticated, (req, res) => {
    prod_db.once("value", function(snapshot) {
        return res.render('cosmetics.ejs', { products: (snapshot.val())['Cosmetics']['Items'] })
        res.end()
    })
})

app.get('/addProduct', checkAuthenticated, (req, res) => {
    prod_db.once("value", function(snapshot) {
        return res.render('addProduct.ejs')
        res.end()
    })
})

app.post('/addProduct', async (req, res) => {
    console.log('addProduct post coming....', req.body.pro_name);
    try {
        // const key = (req.body.pro_name).toString().trim();
        let key = UUID.v4().toString();
        key = key.replace('-','');
        console.log('pro_name as key...',key);
        const addList = {
            name : req.body.pro_name,
            brand : req.body.brand_name,
            price : Number(req.body.pro_price),
            image : req.body.pro_image,
            metrics : req.body.metric_count,
            offer : req.body.offer_percent,
            'expiry date' : req.body.expires,
            description : req.body.description,
            about : req.body.about_product,
            quantity : 1,
            category : 'Grocery',
            id : key,
        }
        await prod_db.child("Grocery").child("Items").update({ [key] : addList })
        return res.redirect('/grocery') 
    } catch {
        console.log("sorry there is some error in access firebase....")
        return res.redirect('/addProduct')
    }

})


app.get('/deleteProduct', async (req, res) => {
    console.log('deleteProduct get coming....', req.query['data2'], req.query['data1']);
    try {
        await prod_db.once("value", function(snapshot) {
            let obj =  snapshot.val();
            let item_id = (req.query['data1']).toString().trim();
            let category = (req.query['data2']).toString().trim();
            if( obj[category] && obj[category]['Items'] && obj[category]['Items'][item_id] )
            {
                console.log("yes its there...",obj[category]['Items'][item_id]);
                prod_db.child(category).child('Items').child(item_id).remove();
            }
            console.log('lol...');
            afterDelete(req,res,category);
        }); 
    } catch {
        console.log("sorry there is some error in access firebase....")
        return res.redirect('/grocery')
    }
    
})


function afterDelete(req,res,category) {
    if(category.toString().trim() === 'Grocery'){
        return res.redirect('/grocery')}
    else if(category.toString().trim() === 'Cosmetics'){
        return res.redirect('/cosmetics')  }
    else if(category.toString().trim() === 'Electronics'){
        return res.redirect('/electronics')   }
    else if(category.toString().trim() === 'Clothes'){
        return res.redirect('/clothes')}
    else{
        console.log('no category found so going to home....')
        return res.redirect('/')
    }              
}

app.post('/settings', (req, res) => {
    console.log('settings post coming....', req.body.u_name);
    try {
        fbdatabase.once("value", function(snapshot) {
            const obj =  snapshot.val();
             console.log("changed check boxes1....",req.body)
            for(let i in obj){
                if(obj[i]['Username'] != null && obj[i]['Username'] === req.body.u_name)
                {
                    console.log("firebase objects.....", obj[i])

                    if(req.body.cosmetics === 'on'){
                        // obj[i]['Products']['Cosmetics'] = 1
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Cosmetics' : 1})
                    }
                    if(req.body.cosmetics == null){
                        // obj[i]['Products']['Cosmetics'] = 0
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Cosmetics' : 0})
                    }
                    if(req.body.grocery === 'on'){
                        // obj[i]['Products']['Grocery'] = 1
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Grocery' : 1})
                    }
                    if(req.body.grocery == null){
                        // obj[i]['Products']['Grocery'] = 0
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Grocery' : 0})
                    }
                    if(req.body.electronics === 'on'){
                        // obj[i]['Products']['Electronics'] = 1
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Electronics' : 1})
                    }
                    if(req.body.electronics == null){
                        // obj[i]['Products']['Electronics'] = 0
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Electronics' : 0})
                    }
                    if(req.body.clothing === 'on'){
                        // obj[i]['Products']['Clothes'] = 1
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Clothes' : 1})
                    }
                    if(req.body.clothing == null){
                        // obj[i]['Products']['Clothes'] = 0
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Clothes' : 0})
                    }
                    if(req.body.carpool === 'on'){
                        // obj[i]['Products']['Clothes'] = 1
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Carpool' : 1})
                    }
                    if(req.body.carpool == null){
                        // obj[i]['Products']['Clothes'] = 0
                        fbdatabase.child(obj[i]['Uid']).child('Products').update({'Carpool' : 0})
                    }
                }
            } 
            console.log("changed check boxes2....",req.body)
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });
        return res.redirect('/settings') 
    } catch {
        console.log("sorry there is some error in access firebase....")
        return res.redirect('/settings')
    }
    
})


app.get('/login', checkNotAuthenticated, (req, res) => {
    console.log('login get varuthu.....',req.session.error )
    return res.render('login.ejs', { error: req.session.error })
    req.session.destroy();
    res.end()
    
    // delete res.session.error;
})

// app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
//     successRedirect: '/',
//     failureRedirect: '/login',
//     failureFlash: true
// }))

app.post('/login', checkNotAuthenticated, (req, res) => {
    console.log('coming da....login post....')
    try{
        // const hashedPassword = await bcrypt.hash(req.body.password, 10)
        firebase.auth().signInWithEmailAndPassword((req.body.email), (req.body.password)).then( (obj) => {
            console.log('Logged in da..',obj.user.uid)
            return res.redirect('/')
            res.end()
        }).catch(e=>{
            console.log('error occurred while logged in da....',e)
            req.session.error = 'Incorrect username or password';
            return res.redirect('/login')
            res.end()
        })
    } catch {
        return res.redirect('/login')
        res.end()
    }

})

app.get('/register', checkNotAuthenticated, (req, res) => {
    res.render('register.ejs')
})

app.post('/register', (req, res) => {
    try {
        firebase.auth().createUserWithEmailAndPassword((req.body.email), (req.body.password)).then( (obj) => {
            console.log('database....',obj.user.uid)
            fbdatabase.child(obj.user.uid).set({ name: req.body.name, email: req.body.email, password:(req.body.password)  }).then( ()=>{
                firebase.auth().signOut().then( () => {
                    console.log('logged out after register.....it will go to login.....')
                    return res.redirect('/login')
                }).catch(e=>{
                    console.log('sorry pa, wait to logout completely....',e)
                })
            }).catch(e=>{
                console.log('error occurred in firebase child da....',e);
            })
        })
            
        
    } catch {
        return res.redirect('/register')
    }
    console.log(users)
})

app.get('/logout', checkAuthenticated, ( req, res ) => {
    console.log('logout called....')
    firebase.auth().signOut().then( () => {
        console.log('logout Successfully')
        res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0')
        res.end()
        res.redirect('back')
        res.end()
        return res.redirect('/login')
        res.end()
    }).catch( e => {
        console.log('log Out error', e)
    })  
})

async function checkAuthenticated(req, res, next) {
    await firebase.auth().onAuthStateChanged(firebaseUser => {
        if(firebaseUser){
            console.log('Logged In authed user no worry go to home...')
            // res.end()
            return next()
        }
        else{
        console.log('Not Logged In firebase auth failed...',firebaseUser)
        // res.end()
        return res.redirect('/login')
        }
    })
}


async function checkNotAuthenticated(req, res, next) {
    await firebase.auth().onAuthStateChanged(firebaseUser => {
        if (firebaseUser) {
            // the below two lines are for redirect the user to the current page itself when its needed
            // res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
            // res.redirect('back');
            console.log('Already Logged In find by firebase ,don\'t use login/register url...')
            return res.redirect('/')
        }
        else{
        console.log('Not Logged In find by firebase, so u can go to login/register page...',firebaseUser)
        return next()
        }
    })    
}


app.listen(3000)